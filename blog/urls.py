from django.conf.urls import url
from django.contrib.sitemaps.views import sitemap
from blog.feed import ArticleFeed
from blog.sitemap import BlogStaticSitemap, BlogArticlesSitemap

sitemaps = {
    'static': BlogStaticSitemap,
    'articles': BlogArticlesSitemap
}

urlpatterns = [
    url(r'^$', 'blog.views.blog_index', name='blog_index'),
    url(r'^feed/$', ArticleFeed()),
    url(r'^subscribe/', 'blog.views.subscribe', name='blog_subscribe'),
    url(r'^tag/$', 'blog.views.blog_redirect', name='blog_redirect'),
    url(r'^tag/(?P<slug>[\w-]+)$', 'blog.views.tag', name='blog_tag'),
    url(r'^(?P<slug>[\w-]+)/', 'blog.views.article_item', name='blog_article_item'),

    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps}, name='blog_sitemap')
]
