from django.shortcuts import render, redirect
from django.db.models import Q
from blog.models import *


def blog_redirect(request):
    return redirect('/blog')


def blog_index(request):
    articles = Article.objects.order_by('-date')
    context = {
        'articles': articles
    }
    return render(request, 'blog/blog_index.html', context)


def article_item(request, slug):
    article = Article.objects.get(slug=slug)
    articles = Article.objects.all()
    articlefortag = Article.objects.filter(tag__article=article).distinct().filter(~Q(id=article.id))
    comments = Comments.objects.filter(article__slug=slug)
    tags = Tags.objects.filter(article__slug=slug)
    context = {
        'article': article,
        'articles': articles,
        'articlefortag': articlefortag,
        'comments': comments,
        'tags': tags,
        'page': article
    }
    return render(request, 'blog/blog_article.html', context)


def tag(request, slug):
    articles = Article.objects.all()
    tagarticles = Article.objects.filter(tag__slug=slug)
    tagtitle = Tags.objects.get(slug=slug)
    context = {
        'articles': articles,
        'tagarticles': tagarticles,
        'tagtitle': tagtitle
    }
    return render(request, 'blog/blog_tag.html', context)


def subscribe(request):
    if request.method == 'POST':
        email = request.POST.get('email', '')
        try:
            Subscribers.objects.get(email=email)
            return render(request, 'blog/blog_subscribe_ajax2.html')
        except:
            subs = Subscribers(email=email)
            subs.save()
    return render(request, 'blog/blog_subscribe_ajax.html')
