# coding:utf8
from django.contrib.syndication.views import Feed
from django.utils.feedgenerator import Rss201rev2Feed
from blog.models import Article


class RssFooFeedGenerator(Rss201rev2Feed):
    def add_root_elements(self, handler):
        super(RssFooFeedGenerator, self).add_root_elements(handler)
        ctx = {
            'url': u"http://bytehouse.ru/static/img/services-site-dev.svg",
            'title': u"Веб-студия BYTEHOUSE",
            'link': u"http://bytehouse.ru/",
            'description': u"Веб-студия BYTEHOUSE"
        }
        handler.addQuickElement(u"image", '', ctx)


class ArticleFeed(Feed):
    feed_type = RssFooFeedGenerator
    title = 'Блог веб-студии BYTEHOUSE'
    link = '/blog/'
    description = 'Статьи о том, как мы делаем дело'
    feed_copyright = 'Copyright (c) 2015, веб-студия BYTEHOUSE'

    # u"image", '',
    #         {
    #              'url': u"http://www.example.com/images/logo.jpg",
    #              'title': u"Some title",
    #              'link': u"http://www.example.com/",
    #          })

    def items(self):
        return Article.objects.order_by('-date')

    def item_author_name(self, item):
        if item.author.firstname:
            return item.author.firstname + ' ' + item.author.lastname
        return item.author.nickname

    def item_author_email(self, item):
        return item.author.contact

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.text

    def item_pubdate(self, item):
        return item.date
