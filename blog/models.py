# coding:utf8
from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField


class Author(models.Model):
    nickname = models.CharField('Ник', max_length=30, blank=True)
    firstname = models.CharField('Имя', max_length=30, blank=True)
    lastname = models.CharField('Фамилия', max_length=30, blank=True)
    avatar = models.ImageField('Аватар', upload_to='blog/author')
    contact = models.CharField('Контакт', max_length=100, blank=True)

    def __unicode__(self):
        if self.firstname:
            return self.firstname + ' ' + self.lastname
        return self.nickname

    class Meta:
        verbose_name = 'автор'
        verbose_name_plural = 'авторы'


class Tags(models.Model):
    title = models.CharField('Тег', max_length=50, unique=True)
    slug = models.CharField('Ссылка', max_length=150, unique=True)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = 'тег'
        verbose_name_plural = 'теги'


class Article(models.Model):
    title = models.CharField('Заголовок', max_length=150)
    slug = models.CharField('Ссылка', max_length=150)
    author = models.ForeignKey(Author, verbose_name='Автор')
    date = models.DateTimeField('Дата', auto_now_add=True)
    shortText = models.CharField('Аннотация', max_length=140)
    text = RichTextUploadingField(verbose_name='Текст', max_length=20000)
    logo = models.ImageField('Лого статьи', upload_to='blog/article/logo')
    background = models.ImageField('Фон статьи', upload_to='blog/article/background', null=True)
    keyArticle = models.BooleanField('Ключевая статья?', default=False)
    tag = models.ManyToManyField(Tags, verbose_name='тег')
    meta_title = models.CharField(u"Title страницы", max_length=200, blank=True)
    meta_keywords = models.CharField(u"Ключевые слова", max_length=1000, blank=True)
    meta_description = models.TextField(u"Meta описание(description)", max_length=2000, blank=True)
    meta_tags = models.TextField(u"Meta", max_length=6000, blank=True)

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return '/blog/%s/' % self.slug

    class Meta:
        verbose_name = 'статья'
        verbose_name_plural = 'статьи'


class Comments(models.Model):
    nickname = models.CharField('Ник', max_length=30)
    email = models.EmailField('Email')
    date = models.DateTimeField('Дата', auto_now_add=True)
    text = models.TextField('Текст', max_length=5000)
    article = models.ForeignKey(Article, verbose_name='статья')

    def __unicode__(self):
        return unicode(self.nickname + u' к ' + self.article.title)

    class Meta:
        verbose_name = 'комментарий'
        verbose_name_plural = 'комментарии'


class Subscribers(models.Model):
    email = models.CharField('Email', max_length=50)

    def __unicode__(self):
        return self.email

    class Meta:
        verbose_name = 'подписчик'
        verbose_name_plural = 'подписчики'