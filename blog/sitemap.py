from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import reverse
from models import Article


class BlogStaticSitemap(Sitemap):
    def items(self):
        return ['blog_index']

    def location(self, obj):
        return reverse(obj)


class BlogArticlesSitemap(Sitemap):
    def items(self):
        return Article.objects.all()

    def lastmod(self, article):
        return article.date
