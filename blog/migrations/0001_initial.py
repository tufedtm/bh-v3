# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=150, verbose_name=b'\xd0\x97\xd0\xb0\xd0\xb3\xd0\xbe\xd0\xbb\xd0\xbe\xd0\xb2\xd0\xbe\xd0\xba')),
                ('slug', models.CharField(max_length=150, verbose_name=b'\xd0\xa1\xd1\x81\xd1\x8b\xd0\xbb\xd0\xba\xd0\xb0')),
                ('date', models.DateTimeField(default=datetime.datetime(2015, 7, 9, 19, 19, 16, 809000), verbose_name=b'\xd0\x94\xd0\xb0\xd1\x82\xd0\xb0')),
                ('shortText', models.CharField(max_length=140, verbose_name=b'\xd0\x90\xd0\xbd\xd0\xbd\xd0\xbe\xd1\x82\xd0\xb0\xd1\x86\xd0\xb8\xd1\x8f')),
                ('text', ckeditor.fields.RichTextField(max_length=20000, verbose_name=b'\xd0\xa2\xd0\xb5\xd0\xba\xd1\x81\xd1\x82')),
                ('logo', models.ImageField(upload_to=b'blog/article/logo', verbose_name=b'\xd0\x9b\xd0\xbe\xd0\xb3\xd0\xbe \xd1\x81\xd1\x82\xd0\xb0\xd1\x82\xd1\x8c\xd0\xb8')),
                ('keyArticle', models.BooleanField(default=False, verbose_name=b'\xd0\x9a\xd0\xbb\xd1\x8e\xd1\x87\xd0\xb5\xd0\xb2\xd0\xb0\xd1\x8f \xd1\x81\xd1\x82\xd0\xb0\xd1\x82\xd1\x8c\xd1\x8f?')),
            ],
            options={
                'verbose_name': '\u0441\u0442\u0430\u0442\u044c\u044f',
                'verbose_name_plural': '\u0441\u0442\u0430\u0442\u044c\u0438',
            },
        ),
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nickname', models.CharField(max_length=30, verbose_name=b'\xd0\x9d\xd0\xb8\xd0\xba', blank=True)),
                ('firstname', models.CharField(max_length=30, verbose_name=b'\xd0\x98\xd0\xbc\xd1\x8f', blank=True)),
                ('lastname', models.CharField(max_length=30, verbose_name=b'\xd0\xa4\xd0\xb0\xd0\xbc\xd0\xb8\xd0\xbb\xd0\xb8\xd1\x8f', blank=True)),
                ('avatar', models.ImageField(upload_to=b'blog/author', verbose_name=b'\xd0\x90\xd0\xb2\xd0\xb0\xd1\x82\xd0\xb0\xd1\x80')),
                ('contact', models.CharField(max_length=100, verbose_name=b'\xd0\x9a\xd0\xbe\xd0\xbd\xd1\x82\xd0\xb0\xd0\xba\xd1\x82', blank=True)),
            ],
            options={
                'verbose_name': '\u0430\u0432\u0442\u043e\u0440',
                'verbose_name_plural': '\u0430\u0432\u0442\u043e\u0440\u044b',
            },
        ),
        migrations.CreateModel(
            name='Comments',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nickname', models.CharField(max_length=30, verbose_name=b'\xd0\x9d\xd0\xb8\xd0\xba')),
                ('email', models.EmailField(max_length=254, verbose_name=b'Email')),
                ('date', models.DateTimeField(default=datetime.datetime(2015, 7, 9, 19, 19, 16, 811000), verbose_name=b'\xd0\x94\xd0\xb0\xd1\x82\xd0\xb0')),
                ('text', models.TextField(max_length=5000, verbose_name=b'\xd0\xa2\xd0\xb5\xd0\xba\xd1\x81\xd1\x82')),
                ('article', models.ForeignKey(verbose_name=b'\xd1\x81\xd1\x82\xd0\xb0\xd1\x82\xd1\x8c\xd1\x8f', to='blog.Article')),
            ],
            options={
                'verbose_name': '\u043a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439',
                'verbose_name_plural': '\u043a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0438',
            },
        ),
        migrations.CreateModel(
            name='Tags',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(unique=True, max_length=50, verbose_name=b'\xd0\xa2\xd0\xb5\xd0\xb3')),
                ('slug', models.CharField(unique=True, max_length=150, verbose_name=b'\xd0\xa1\xd1\x81\xd1\x8b\xd0\xbb\xd0\xba\xd0\xb0')),
            ],
            options={
                'verbose_name': '\u0442\u0435\u0433',
                'verbose_name_plural': '\u0442\u0435\u0433\u0438',
            },
        ),
        migrations.AddField(
            model_name='article',
            name='author',
            field=models.ForeignKey(verbose_name=b'\xd0\x90\xd0\xb2\xd1\x82\xd0\xbe\xd1\x80', to='blog.Author'),
        ),
        migrations.AddField(
            model_name='article',
            name='tag',
            field=models.ManyToManyField(to='blog.Tags', verbose_name=b'\xd1\x82\xd0\xb5\xd0\xb3'),
        ),
    ]
