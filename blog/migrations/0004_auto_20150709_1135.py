# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_auto_20150709_1134'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2015, 7, 9, 11, 35, 54, 955645), verbose_name=b'\xd0\x94\xd0\xb0\xd1\x82\xd0\xb0'),
        ),
        migrations.AlterField(
            model_name='article',
            name='shortText',
            field=models.CharField(max_length=140, verbose_name=b'\xd0\x90\xd0\xbd\xd0\xbd\xd0\xbe\xd1\x82\xd0\xb0\xd1\x86\xd0\xb8\xd1\x8f'),
        ),
        migrations.AlterField(
            model_name='comments',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2015, 7, 9, 11, 35, 54, 959422), verbose_name=b'\xd0\x94\xd0\xb0\xd1\x82\xd0\xb0'),
        ),
    ]
