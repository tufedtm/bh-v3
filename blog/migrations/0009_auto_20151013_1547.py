# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0008_auto_20150803_1805'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='meta_description',
            field=models.TextField(max_length=2000, verbose_name='Meta \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435(description)', blank=True),
        ),
        migrations.AddField(
            model_name='article',
            name='meta_keywords',
            field=models.CharField(max_length=1000, verbose_name='\u041a\u043b\u044e\u0447\u0435\u0432\u044b\u0435 \u0441\u043b\u043e\u0432\u0430', blank=True),
        ),
        migrations.AddField(
            model_name='article',
            name='meta_tags',
            field=models.TextField(max_length=6000, verbose_name='Meta', blank=True),
        ),
        migrations.AddField(
            model_name='article',
            name='meta_title',
            field=models.CharField(max_length=200, verbose_name='Title \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b', blank=True),
        ),
    ]
