from django.contrib import admin
from blog.models import *


# class TagsInline(admin.TabularInline):
#     prepopulated_fields = {'slug': ('title',)}
#     model = Tags
#     extra = 1


class ArticleAdmin(admin.ModelAdmin):
    # inlines = [TagsInline]
    prepopulated_fields = {'slug': ('title',)}


class TagsAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(Article, ArticleAdmin)
admin.site.register(Author)
admin.site.register(Comments)
admin.site.register(Tags, TagsAdmin)
admin.site.register(Subscribers)
