$(document).ready(function () {


  $('#subscribe').submit(function () {
    var email = $('#subs_email').val();
    subscribe_ajax(email);
    return false;
  });


  (function () {
    $("[type=tel]").mask("+7 (999) 999-9999");

    $.slidebars();

    $(".main__header-xs-menu").click(function (e) {
      e.preventDefault();
    });

    $('.modal_btn').magnificPopup({
      type: 'inline',
      preloader: false,
      focus: '#name',
      callbacks: {
        beforeOpen: function () {
          if ($(window).width() < 700) {
            this.st.focus = false;
          } else {
            this.st.focus = '#name';
          }
        }
      }
    });


    // portfolio

    $(window).load(function () {
      if (window.innerWidth > 600) {
        var $pf = $('.portfolio-items').isotope({
          itemSelector: '.portfolio-item',
          layoutMode: 'packery',
          percentPosition: true
        });
      } else if (window.innerWidth <= 600) {
        Isotope.Item.prototype._create = function () {
          // assign id, used for original-order sorting
          this.id = this.layout.itemGUID++;
          // transition objects
          this._transn = {
            ingProperties: {},
            clean: {},
            onEnd: {}
          };
          this.sortData = {};
        };

        Isotope.Item.prototype.layoutPosition = function () {
          this.emitEvent('layout', [this]);
        };

        Isotope.prototype.arrange = function (opts) {
          // set any options pass
          this.option(opts);
          this._getIsInstant();
          // just filter
          this.filteredItems = this._filter(this.items);
          // flag for initalized
          this._isLayoutInited = true;
        };

        var NoneMode = Isotope.LayoutMode.create('none');

        var $pf = $('.portfolio-items').isotope({
          itemSelector: '.portfolio-item',
          layoutMode: 'none',
          percentPosition: true
        });
      }

      $('.portfolio_side button').click(function () {

        var filterValue = $(this).attr('data-filter');
        //filterValue = filterFns[filterValue] || filterValue;
        $pf.isotope({
          filter: filterValue
        });


        $('.portfolio_side button').removeClass();
        $(this).addClass('active');
        return false;
      });
    });
    //////


    //pfi top slider

    $('#pfi-t-s').lightSlider({
      item: 1,
      mode: 'fade',
      auto: true,
      loop: true,
      speed: 1500,
      pause: 5000,
      controls: false,
      addClass: 'pfi-t-s'
    });

    $('#pfi-slider-fw').lightSlider({
      item: 1,
      mode: 'fade',
      loop: true,
      speed: 1500,
      pause: 5000,
      controls: false,
      addClass: 'pfi-slider-fw'
    });

    $('#pfi-slider-hw').lightSlider({
      item: 1,
      mode: 'fade',
      loop: true,
      speed: 1500,
      pause: 5000,
      controls: false,
      addClass: 'pfi-slider-hw'
    });

    /////////


  }());
});