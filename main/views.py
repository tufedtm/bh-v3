from django.shortcuts import render, get_object_or_404
from .models import ContactForm, StaticPages
from .models import Pages
from blog.models import Article


def index(request):
    page = get_object_or_404(Pages, slug="home")
    return render(request, 'index.html', {'page': page})


def pages(request, slug):
    if slug == 'blog':
        articles = Article.objects.order_by('-date')
        page = get_object_or_404(Pages, slug=slug)
        context = {
            'articles': articles,
            'page': page
        }
        return render(request, 'blog/blog_index.html', context)
    page = get_object_or_404(Pages, slug=slug)
    return render(request, page.template, {'page': page})


def ajax(request):
    if request.method == 'POST':
        name = request.POST.get('name', '')
        tel = request.POST.get('tel', '')
        email = request.POST.get('email', '')
        desc = request.POST.get('desc', '')
        cont = ContactForm(name=name, tel=tel, email=email, desc=desc)
        cont.save()
    return render(request, 'ajax.html')


def static_pages(request, slug):
    page = StaticPages.objects.get(url=slug)
    context = {
        'page': page
    }
    return render(request, 'static_page.html', context)


# todo: delete
def pfi(request):
    return render(request, 'portfolio_item.html')
