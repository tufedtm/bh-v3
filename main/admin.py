from django.contrib import admin
from .models import ContactForm, StaticPages, Pages


class ContactFormAdmin(admin.ModelAdmin):
    list_display = ('name', 'date')


class StaticPagesAdmin(admin.ModelAdmin):
    list_display = ('title', 'url', 'sort', 'on', 'meta_title', 'meta_keywords', 'meta_description')
    list_editable = ('sort', 'meta_title', 'on', 'meta_keywords', 'meta_description')
    prepopulated_fields = {'url': ('title',)}


class PagesAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'active', 'on', 'on_menu', 'sort', 'meta_title', 'meta_keywords', 'meta_description')
    list_editable = ('active', 'on', 'on_menu', 'sort', 'meta_title', 'meta_keywords', 'meta_description')
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(ContactForm, ContactFormAdmin)
admin.site.register(StaticPages, StaticPagesAdmin)
admin.site.register(Pages, PagesAdmin)
