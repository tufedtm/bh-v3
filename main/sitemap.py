from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import reverse
from .models import Pages, StaticPages
from blog.models import Article

class StaticSitemap(Sitemap):
    def items(self):
        return ['main-index']

    def location(self, obj):
        return reverse(obj)


class PagesSitemap(Sitemap):
    def items(self):
        return Pages.page_object.exclude(slug='base').exclude(slug='home').exclude(on=True)


class StaticPagesSitemap(Sitemap):
    def items(self):
        return StaticPages.objects.exclude(on=True)


class ArticleSitemap(Sitemap):
    def items(self):
        return Article.objects.all()
