# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0006_auto_20151014_1443'),
    ]

    operations = [
        migrations.AddField(
            model_name='pages',
            name='on',
            field=models.BooleanField(default=False, verbose_name='\u041e\u0442\u043a\u043b\u044e\u0447\u0438\u0442\u044c \u0441\u0441\u044b\u043b\u043a\u0443'),
        ),
        migrations.AlterField(
            model_name='pages',
            name='template',
            field=models.CharField(max_length=100, verbose_name='\u0428\u0430\u0431\u043b\u043e\u043d', choices=[(b'index.html', b'\xd0\x93\xd0\xbb\xd0\xb0\xd0\xb2\xd0\xbd\xd0\xb0\xd1\x8f - index.html'), (b'default.html', b'\xd0\x94\xd0\xb5\xd1\x84\xd0\xbe\xd0\xbb\xd1\x82\xd0\xbd\xd0\xb0\xd1\x8f - default.html'), (b'blog/blog_index.html', b'\xd0\x91\xd0\xbb\xd0\xbe\xd0\xb3 - blog/blog_index'), (b'contacts.html', b'\xd0\x9a\xd0\xbe\xd0\xbd\xd1\x82\xd0\xb0\xd0\xba\xd1\x82\xd1\x8b - contacts.html')]),
        ),
    ]
