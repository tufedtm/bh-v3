# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor_uploader.fields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_auto_20150803_1805'),
    ]

    operations = [
        migrations.AlterField(
            model_name='staticpages',
            name='text',
            field=ckeditor_uploader.fields.RichTextUploadingField(max_length=20000, verbose_name=b'\xd0\xa2\xd0\xb5\xd0\xba\xd1\x81\xd1\x82'),
        ),
    ]
