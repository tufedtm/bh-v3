# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0008_auto_20151015_1549'),
    ]

    operations = [
        migrations.AddField(
            model_name='staticpages',
            name='on',
            field=models.BooleanField(default=False, verbose_name='\u041e\u0442\u043a\u043b\u044e\u0447\u0438\u0442\u044c \u0441\u0441\u044b\u043b\u043a\u0443'),
        ),
        migrations.AlterField(
            model_name='staticpages',
            name='sort',
            field=models.SmallIntegerField(default=1, verbose_name='\u041f\u043e\u0437\u0438\u0446\u0438\u044f'),
        ),
        migrations.AlterField(
            model_name='staticpages',
            name='title',
            field=models.CharField(max_length=140, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a'),
        ),
        migrations.AlterField(
            model_name='staticpages',
            name='url',
            field=models.SlugField(max_length=200, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430'),
        ),
    ]
