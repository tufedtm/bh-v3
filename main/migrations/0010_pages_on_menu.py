# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0009_auto_20151015_1553'),
    ]

    operations = [
        migrations.AddField(
            model_name='pages',
            name='on_menu',
            field=models.BooleanField(default=True, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c \u0432 \u0413\u043b\u0430\u0432\u043d\u043e\u043c \u043c\u0435\u043d\u044e'),
        ),
    ]
