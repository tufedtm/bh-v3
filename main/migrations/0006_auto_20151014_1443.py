# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor_uploader.fields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_auto_20151013_1715'),
    ]

    operations = [
        migrations.CreateModel(
            name='Pages',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text='\u043e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0435\u0442\u0441\u044f \u0438 \u0432 \u041c\u0435\u043d\u044e', max_length=100, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b')),
                ('slug', models.SlugField(max_length=100, verbose_name='url')),
                ('active', models.BooleanField(default=True, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0443')),
                ('sort', models.SmallIntegerField(default=1, verbose_name='\u041f\u043e\u0437\u0438\u0446\u0438\u044f')),
                ('template', models.CharField(max_length=100, verbose_name='\u0428\u0430\u0431\u043b\u043e\u043d', choices=[(b'index.html', b'\xd0\x93\xd0\xbb\xd0\xb0\xd0\xb2\xd0\xbd\xd0\xb0\xd1\x8f - index.html'), (b'default.html', b'\xd0\x93\xd0\xbb\xd0\xb0\xd0\xb2\xd0\xbd\xd0\xb0\xd1\x8f - default.html'), (b'contacts.html', b'\xd0\x9a\xd0\xbe\xd0\xbd\xd1\x82\xd0\xb0\xd0\xba\xd1\x82\xd1\x8b - contacts.html')])),
                ('content', ckeditor_uploader.fields.RichTextUploadingField(max_length=15000, verbose_name='\u041a\u043e\u043d\u0442\u0435\u043d\u0442', blank=True)),
                ('post_content', ckeditor_uploader.fields.RichTextUploadingField(max_length=15000, verbose_name='\u041a\u043e\u043d\u0442\u0435\u043d\u0442 \u0441\u043d\u0438\u0437\u0443', blank=True)),
                ('meta_title', models.CharField(max_length=200, verbose_name='Title \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b', blank=True)),
                ('meta_keywords', models.CharField(max_length=1000, verbose_name='\u041a\u043b\u044e\u0447\u0435\u0432\u044b\u0435 \u0441\u043b\u043e\u0432\u0430', blank=True)),
                ('meta_description', models.TextField(max_length=2000, verbose_name='Meta \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435(description)', blank=True)),
                ('meta_tags', models.TextField(max_length=6000, verbose_name='Meta', blank=True)),
                ('scripts', models.TextField(max_length=6000, verbose_name='\u0421\u043a\u0440\u0438\u043f\u0442\u044b', blank=True)),
            ],
            options={
                'ordering': ['sort', 'id'],
                'verbose_name': '\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u0430',
                'verbose_name_plural': '\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u044b',
            },
        ),
        migrations.AddField(
            model_name='staticpages',
            name='meta_description',
            field=models.TextField(max_length=2000, verbose_name='Meta \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435(description)', blank=True),
        ),
        migrations.AddField(
            model_name='staticpages',
            name='meta_keywords',
            field=models.CharField(max_length=1000, verbose_name='\u041a\u043b\u044e\u0447\u0435\u0432\u044b\u0435 \u0441\u043b\u043e\u0432\u0430', blank=True),
        ),
        migrations.AddField(
            model_name='staticpages',
            name='meta_tags',
            field=models.TextField(max_length=6000, verbose_name='Meta', blank=True),
        ),
        migrations.AddField(
            model_name='staticpages',
            name='meta_title',
            field=models.CharField(max_length=200, verbose_name='Title \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b', blank=True),
        ),
        migrations.AddField(
            model_name='staticpages',
            name='scripts',
            field=models.TextField(max_length=6000, verbose_name='\u0421\u043a\u0440\u0438\u043f\u0442\u044b', blank=True),
        ),
    ]
