from django.conf.urls import url
from django.contrib.sitemaps.views import sitemap
from django.views.generic import TemplateView
from main.sitemap import StaticPagesSitemap, StaticSitemap, PagesSitemap, ArticleSitemap

sitemaps = {
    'static': StaticSitemap,
    'pages': PagesSitemap,
    'static_pages': StaticPagesSitemap,
    'article': ArticleSitemap
}

urlpatterns = [
    url(r'^$', 'main.views.index', name='main-index'),

    url(r'^about/(?P<slug>[\w-]+)$', 'main.views.static_pages', name='main-static_pages'),

    # todo: delete
    url(r'^pfi$', 'main.views.pfi', name='pfi'),

    url(r'^ajax/', 'main.views.ajax', name='main-ajax'),

    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps}),
    url(r'^robots\.txt$', TemplateView.as_view(template_name="robots.txt")),
    url(r'^(?P<slug>[\w-]+)$', 'main.views.pages', name='pages'),
]
