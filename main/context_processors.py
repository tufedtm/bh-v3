from .models import Pages, StaticPages


def menu(request):
    menus = Pages.page_object.exclude(slug='home')
    static_menus = StaticPages.objects.all()
    page_base = Pages.objects.get(pk=1)
    return {
        'menus': menus, 'page_base': page_base,
        'static_menus': static_menus
    }
