# coding: utf8
from django.db import models
from django.db.models.manager import Manager
from ckeditor_uploader.fields import RichTextUploadingField


TEXT = """<meta name="keywords" content="">
<meta name="description" content="">"""

urlik = '/'


class ContactForm(models.Model):
    name = models.CharField('Имя', max_length=100)
    tel = models.CharField('Телефон', max_length=20)
    email = models.CharField('Email', max_length=50)
    desc = models.TextField('Описание', max_length=5000)
    date = models.DateTimeField('Дата', auto_now_add=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = 'заявка'
        verbose_name_plural = 'заявки'


class StaticPages(models.Model):
    title = models.CharField(verbose_name=u'Заголовок', max_length=140)
    url = models.SlugField(verbose_name=u'Ссылка', max_length=200)
    sort = models.SmallIntegerField(u'Позиция', default=1)
    on = models.BooleanField(u'Отключить ссылку', default=False)
    text = RichTextUploadingField(verbose_name='Текст', max_length=20000)
    meta_title = models.CharField(u"Title страницы", max_length=200, blank=True)
    meta_keywords = models.CharField(u"Ключевые слова", max_length=1000, blank=True)
    meta_description = models.TextField(u"Meta описание(description)", max_length=2000, blank=True)
    meta_tags = models.TextField(u"Meta", max_length=6000, blank=True)
    scripts = models.TextField(u"Скрипты", max_length=6000, blank=True)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = 'Статическая страница'
        verbose_name_plural = 'Статические страницы'
        ordering = ['sort', 'id']

    def get_absolute_url(self):
        return urlik + 'about/%s' % self.url


TEMPLATES = (
    ('index.html', 'Главная - index.html'),
    ('default.html', 'Дефолтная - default.html'),
    ('portfolio.html', 'Портфолио - portfolio.html'),
    ('blog/blog_index.html', 'Блог - blog/blog_index'),
    ('contacts.html', 'Контакты - contacts.html'),
)


class ActiveManager(models.Manager):
    def get_queryset(self):
        return super(ActiveManager, self).get_queryset().filter(active=True)


class Pages(models.Model):
    title = models.CharField(u"Название страницы", max_length=100, help_text=u"отображается и в Меню")
    slug = models.SlugField(u"url", max_length=100)
    active = models.BooleanField(u"Отображать страницу", default=True)
    on = models.BooleanField(u"Отключить ссылку", default=False)
    on_menu = models.BooleanField(u"Отображать в Главном меню", default=True)
    sort = models.SmallIntegerField(u"Позиция", default=1)
    template = models.CharField(u"Шаблон", max_length=100, choices=TEMPLATES)
    content = RichTextUploadingField(u"Контент", max_length=15000, blank=True)
    post_content = RichTextUploadingField(u"Контент снизу", max_length=15000, blank=True)
    meta_title = models.CharField(u"Title страницы", max_length=200, blank=True)
    meta_keywords = models.CharField(u"Ключевые слова", max_length=1000, blank=True)
    meta_description = models.TextField(u"Meta описание(description)", max_length=2000, blank=True)
    meta_tags = models.TextField(u"Meta", max_length=6000, blank=True)
    scripts = models.TextField(u"Скрипты", max_length=6000, blank=True)

    objects = Manager()
    page_object = ActiveManager()

    def get_template(self):
        return self.template

    class Meta:
        verbose_name = u'Страница'
        verbose_name_plural = u'Страницы'
        ordering = ['sort', 'id']

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return urlik + '%s' % self.slug
